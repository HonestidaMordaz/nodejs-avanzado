var http = require("http");
var conf = require("./conf");
var expressServer = require("./app/expressServer");

var app = new expressServer();
var server = http.createServer(app.expressServer);

server.listen(conf.port);
console.log("Server listening at localhost:8080");