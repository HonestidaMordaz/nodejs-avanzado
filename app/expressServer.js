var env = process.env.NODE_ENV || "production";
var express = require("express");
var middlewares = require("./middlewares/admin");
var swig = require("swig");

var ExpressServer = function(coonfig) {
	config = config || {};

	this.expressServer = express();

	// middlewares
	for(var middleware in middlewares){
		this.expressServer.use(middlewares[middleware]);
	}

	this.expressServer.engine("html", swig.render);
	this.expressServer.set("view engine", "html");
	this.expressServer.set("view", __dirname + "/website/views/templates");
	swig.setDefaults({ varControls:["[[", "]]"] });

	if(env == "developement"){
		console.log("NO cache");
		this.expressServer.set("view cache", false);
		swig.setDefaults({ cache: false, varControls:["[[", "]]"] });
	}

	this.expressServer.get("/article/list", function(req, res, next){
		res.render("article_list", {});
	});

	this.expressServer.get("/article/save", function(req, res, next){
		res.render("article_save", {nombre:"Junito"});
	});
};

module.exports = ExpressServer;